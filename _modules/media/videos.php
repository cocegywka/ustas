  <article class="container inner-txt media-gallery">

    <div id="galleria-controls" class="galleria-component clearfix">
      <ul id="galleria-content-type" class="media-nav _tabs">
        <li data-type="pages/media" class="active">
          <a href="/pages/media/videos.php" class="media-nav_lnk">Videos</a>
        </li>
        <li data-type="pages/media">
          <a href="/pages/media/images.php" class="media-nav_lnk">Images</a>
        </li>
      </ul>

      <ul class="functions">
        <li class="play"><span>Play</span></li>
        <li class="fullscreen"><span>Fullscreen</span></li>
      </ul>
    </div>

    <div id="galleria-content" class="galleria-has-age-gates">
      <ul id="media-gallery-image-list">
        <li>
          <a 
          href="/"
          data-title="Video preview 1"
          data-description="image description here 1"
          data-description=""
          data-id="videos.php"
          data-tags=""
          data-video="http://www.youtube.com/watch?v=Mzppr3BQUXA"
          data-game-title="Titanfall"
          data-url-id=""
          data-thumb="/uploads/videos/thumbs/preview.jpg"
          data-video-link = ""
          >XXXELFXXX - Video preview</a>
        </li>
        <li>
          <a 
          href="/"
          data-title="Video preview 1"
          data-description="image description here 1"
          data-description=""
          data-id="videos.php"
          data-tags=""
          data-video="http://www.youtube.com/watch?v=Mzppr3BQUXA"
          data-game-title="Titanfall"
          data-url-id=""
          data-thumb="/uploads/videos/thumbs/preview.jpg"
          data-video-link = ""
          >XXXELFXXX - Video preview</a>
        </li>
        <li>
          <a 
          href="/"
          data-title="Video preview 1"
          data-description="image description here 1"
          data-description=""
          data-id="videos.php"
          data-tags=""
          data-video="http://www.youtube.com/watch?v=Mzppr3BQUXA"
          data-game-title="Titanfall"
          data-url-id=""
          data-thumb="/uploads/videos/thumbs/preview.jpg"
          data-video-link = ""
          >XXXELFXXX - Video preview</a>
        </li>
        <li>
          <a 
          href="/"
          data-title="Video preview 1"
          data-description="image description here 1"
          data-description=""
          data-id="videos.php"
          data-tags=""
          data-video="http://www.youtube.com/watch?v=Mzppr3BQUXA"
          data-game-title="Titanfall"
          data-url-id=""
          data-thumb="/uploads/videos/thumbs/preview.jpg"
          data-video-link = ""
          >XXXELFXXX - Video preview</a>
        </li>
        <li>
          <a 
          href="/"
          data-title="Video preview 1"
          data-description="image description here 1"
          data-description=""
          data-id="videos.php"
          data-tags=""
          data-video="http://www.youtube.com/watch?v=Mzppr3BQUXA"
          data-game-title="Titanfall"
          data-url-id=""
          data-thumb="/uploads/videos/thumbs/preview.jpg"
          data-video-link = ""
          >XXXELFXXX - Video preview</a>
        </li>
      </ul>
    </div>

    <div id="galleria-view-trigger" class="galleria-component"></div>

  </article>
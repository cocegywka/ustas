<? $lnk_styles = array('nav_deffault' => '','nav_active' => 'active');?>

<nav id="primary-nav" class="navbar" role="navigation">
    <div class="container primary-nav_inner">
        <div class="row-fluid rel">
            <div id="socLinks" class="socMenu col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 text-right">
                <a href="https://twitter.com" class="socMenu_i __tw"></a>
                <a href="https://twitter.com" class="socMenu_i __twr"></a>
                <a href="http://facebook.com" class="socMenu_i __fc"></a>
            </div><!-- end/#socialBar -->
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="logo">
                    <a class="logo_lnk" href="index.php">XXXELFXXX - Sexcraft</a>
                </div>
            </div><!-- end/.navbar-header -->
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="headerNavbarCollapse">
                <ul class="nav navbar-nav navbar-right">
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'index')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$lnk_styles[$btn_key];?>"><a href="/index.php">Home</a></li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'characters')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$lnk_styles[$btn_key];?>"><a href="/characters.php">Characters</a></li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'media')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$lnk_styles[$btn_key];?>"><a href="/pages/media/videos.php">Media</a></li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'login')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$lnk_styles[$btn_key];?>"><a href="/login.php">Login</a></li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'join')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$lnk_styles[$btn_key];?>"><a class="header_gold" href="/join.php">Join Now</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </div>
</nav>
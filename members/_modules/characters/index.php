	<h1>A Glimpse of Felther's Heroines</h1>
	<hr>
	<ul class="ugrid">
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Felitia</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Vientra</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Trime</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Valorah</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Feryl</h3>
				</div>
			</a>
		</li>
	</ul>
	<ul class="ugrid">
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Tweyani</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr twoRows">Thalia &<br> Omyra</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Opiette</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Divya</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Hydrea</h3>
				</div>
			</a>
		</li>
	</ul>
	<ul class="ugrid">
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Terrawyn</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr twoRows">Parah &<br> Salin</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Weave</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr twoRows">Queen<br> Noscrume</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="character_single.php" class="thumbnail charCol">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Extras</h3>
				</div>
			</a>
		</li>
	</ul>
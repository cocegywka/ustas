	<h1>Character Name</h1>
	<hr>

	<ul class="nav nav-tabs media-nav" role="tablist">
		<li class="active">
			<a href="#info" class="media-nav_lnk" role="tab" data-toggle="pill">Info</a>
		</li>
		<li>
			<a href="#background_info" class="media-nav_lnk" role="tab" data-toggle="pill">Background Info</a>
		</li>
	</ul>

	<div class="row chars-tab-theme">
		<div class="col-sm-4 col-sm-push-8 col-md-3 col-md-push-9 tab-aside">
			<img src="/uploads/characters/preview.png" alt="">
			<div class="caption mask">
				<h3 class="char_hdr">Feryl</h3>
			</div>
		</div>
		<div class="col-sm-8 col-sm-pull-4 col-md-9 col-md-pull-3">
			<div class="tab-content">
				<div class="tab-pane fade in active" id="info">
					<div class="row char-info">
						<div class="col-xs-6 col-md-4">
							<ul class="list-unstyled info-ul">
								<li>Name: <b>Feryl</b></li>
								<li>Race: <b>Unknown Elf</b></li>
								<li>Class: <b>Dragonrider</b></li>
								<li>Armor Class: <b>257</b></li>
								<li>Age: <b>675</b></li>
								<li>Measurements: <b>34 - 30 - 34</b></li>
								<li>Height: <b>123</b></li>
								<li>Favorite Position: <b>Dragon Style</b></li>
							</ul>
						</div>
						<div class="col-xs-6 col-md-4">
							<ul class="list-unstyled info-ul">
								<li>Intelligence: <b>139</b></li>
								<li>Dexterity: <b>202</b></li>
								<li>Charisma: <b>102</b></li>
								<li>Strength: <b>265</b></li>
								<li>Agility: <b>187</b></li>
								<li>Wisdom: <b>182</b></li>
								<li>Intuition: <b>131</b></li>
								<li>Stamina: <b>272</b></li>
							</ul>
						</div>
					</div>
					<p>Likes/Turn ons: <b>Fire, riding into battle on her Siobhan, toughness, strength, lovers with an animalistic drive and stamina to match</b></p>
					<p>Dislikes/Turn offs: <b>Water, gentle caresses, soft whispers, and salads</b></p>
				</div>

				<div class="tab-pane fade" id="background_info">
					<p>Raised under the primal instincts of the Dragons has made Feryl unrefined amongst Elven company. Impatient with chitchat, she prefers the company of her sister dragon, Saibon, with whom she telepathically communicates. Her suitors must be of clear mind and vigorous stamina to compete with the thrill of flying to the boundaries of The Great Empty. With the emotional isolation of being the Dragonrider, she seeks out lovers where she can succumb to their touch, exploring the reaches of her heart's desire to submit. With the scales Saibon, fused to her shoulders as armor, she wields a two handed sword as an extension of herself. Feryl seeks to decimate the Tonnomas and Marwalt for corrupting the expansive vistas of Felther, bringing forth the age of cleansing and renewal.</p>
				</div>
			</div>
		</div>
	</div>

	<h1>Image Galleries & Videos</h1>
	<hr>

	<div class="row char_imgvid">
		<div class="col-xs-6 col-sm-3 col-md-3">
			<a href="media_images.php" class="thumbnail">
				<div class="thumbnail_img">
					<i class="action_i __gallery"></i>
					<img src="/uploads/galleries/preview1.jpg" alt="">
				</div>
				<div class="caption">
					<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
					<p class="_txt">Preview of minigame "Solara's Plasma".</p>
					<div class="rateStars rate_5">
						<i class="rate_i"></i>
						<i class="rate_i __on"></i>
					</div>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 col-md-3">
			<a href="media_images.php" class="thumbnail">
				<div class="thumbnail_img">
					<i class="action_i __gallery"></i>
					<img src="/uploads/galleries/preview1.jpg" alt="">
				</div>
				<div class="caption">
					<h5 class="media_hdr">Minigame Preview: Qora's Court</h5>
					<p class="_txt">A preview of our minigame "Qora's Court".</p>
					<div class="rateStars rate_5">
						<i class="rate_i"></i>
						<i class="rate_i __on"></i>
					</div>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 col-md-3">
			<a href="media_images.php" class="thumbnail">
				<div class="thumbnail_img">
					<i class="action_i __gallery"></i>
					<img src="/uploads/galleries/preview1.jpg" alt="">
				</div>
				<div class="caption">
					<h5 class="media_hdr">Strip Interactives Gallery</h5>
					<p class="_txt">Full size images of the Strip Interactives elves.</p>
					<div class="rateStars rate_5">
						<i class="rate_i"></i>
						<i class="rate_i __on"></i>
					</div>
				</div>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 col-md-3">
			<a href="media_videos.php" class="thumbnail">
				<div class="thumbnail_img">
					<i class="action_i __play"></i>
					<img src="/uploads/videos/thumbs/preview.jpg" alt="">
				</div>
				<div class="caption">
					<h5 class="media_hdr">Episode 4: Valorah's Blessing</h5>
					<p class="_txt">An encounter in eastern Omonia.</p>
					<div class="rateStars rate_1">
						<i class="rate_i"></i>
						<i class="rate_i __on"></i>
					</div>
				</div>
			</a>
		</div>
	</div>

	<h1>Felther's Heroines</h1>
	<hr>

	<div class="characters-heroes">
		<div class="row">
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Felitia</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Vientra</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Trime</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Valorah</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Feryl</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Tweyani</h3>
					</div>
				</a>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-4 col-md-2 col-md-offset-1">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr twoRows">Thalia &<br> Omyra</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Opiette</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Divya</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Hydrea</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Terrawyn</h3>
					</div>
				</a>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-4 col-md-2 col-md-offset-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr twoRows">Parah &<br> Salin</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Weave</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr twoRows">Queen<br> Noscrume</h3>
					</div>
				</a>
			</div>
			<div class="col-xs-4 col-md-2">
				<a href="character_single.php" class="thumbnail charCol">
					<img src="/uploads/characters/mini_preview.png" alt="">
					<div class="caption mask">
						<h3 class="char_hdr">Extras</h3>
					</div>
				</a>
			</div>
		</div>
	</div>
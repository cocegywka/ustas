<article>
<div class="pro-main join">

            <h3 class="intro_heading">Get access to our world</h3>
            <hr class="txtDev">
            <p class="lead __1">You're just click away from anstrstricted access!</p>

            <div class="joinGroup center-block">

                <div class="row-fluid">
                    <a href="#" class="btn btn-primary purchase_a">
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <h4 class="purchase_hdr">1 Month Membership</h4>
                            <p class="purchase_txt">with monthly rebilling $8.95</p>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <span class="purchase_price">$19.95</span>
                        </div>
                    </a>
                </div>

                <div class="row-fluid">
                    <a href="#" class="btn btn-primary purchase_a">
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <h4 class="purchase_hdr">6 Month Membership</h4>
                            <p class="purchase_txt">One time Charge - NO rebilling</p>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <span class="purchase_price">$54.95</span>
                        </div>
                    </a>
                </div>

                <div class="row-fluid">
                    <a href="#" class="btn btn-primary purchase_a">
                        <div class="col-xs-8 col-sm-8 col-md-8">
                            <h4 class="purchase_hdr">1 year Membership</h4>
                            <p class="purchase_txt">One time Charge - NO rebilling</p>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <span class="purchase_price">$99.95</span>
                        </div>
                    </a>
                </div>

            </div>

            <h5 class="hdr">Please visit <a class="gld_lnk" href="http://epoch.com">epoch.com</a>, our authorized sales agent.</h5>
</div>

<div class="pro-main joinThumbs">

  <div class="row joinThumbs_row">
    <div class="col-xs-6 col-md-3 joinThumbs_col">
      <a href="#" class="joinThumbs_lnk">
        <img src="/uploads/news/preview.jpg" alt="">
      </a>
    </div>
    <div class="col-xs-6 col-md-3 joinThumbs_col">
      <a href="#" class="joinThumbs_lnk">
        <img src="/uploads/news/preview.jpg" alt="">
      </a>
    </div>
    <div class="col-xs-6 col-md-3 joinThumbs_col">
      <a href="#" class="joinThumbs_lnk">
        <img src="/uploads/news/preview.jpg" alt="">
      </a>
    </div>
    <div class="col-xs-6 col-md-3 joinThumbs_col">
      <a href="#" class="joinThumbs_lnk">
        <img src="/uploads/news/preview.jpg" alt="">
      </a>
    </div>
  </div>

</div>
</article>
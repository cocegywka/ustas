	<div class="row">
		<div class="col-md-10 content-column">
			<div>
				<h1>Latest Video</h1>
				<hr>
				<div class="row">
					<div class="col-xs-6 col-sm-3 col-md-3">
						<div class="thumbnail">
							<a href="media_videos.php" class="thumbnail_img">
								<i class="action_i __play"></i>
								<img src="/uploads/videos/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">Ari's Amulet Preview</h5>
								<p class="_txt">A preview of the upcoming video entitled "Ari's Amulet".</p>
								<div class="rateStars rate_5">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<div class="thumbnail">
							<a href="media_videos.php" class="thumbnail_img">
								<i class="action_i __play"></i>
								<img src="/uploads/videos/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">Zelwynn's Charge</h5>
								<p class="_txt">Zelwynn sucks you into the pleasures of life in Xtacia.</p>
								<div class="rateStars rate_3">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<div class="thumbnail">
							<a href="media_videos.php" class="thumbnail_img">
								<i class="action_i __play"></i>
								<img src="/uploads/videos/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">A Sensual Faerie</h5>
								<p class="_txt">Amarok encounters a sexy sprite on a trip down an unlikely path.</p>
								<div class="rateStars rate_5">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<div class="thumbnail">
							<a href="media_videos.php" class="thumbnail_img">
								<i class="action_i __play"></i>
								<img src="/uploads/videos/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">Episode 8: Tweyani's Revenge</h5>
								<p class="_txt">The Plains Elf warrior unleashes her inner beast.</p>
								<div class="rateStars rate_4">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<a class="btn btn-trans btn-block" href="media.php#videos">View All Videos</a>
			</div>

			<div>
				<h1>Games</h1>
				<hr class="linear_hr">
				<div class="row games_row">
					<div class="col-xs-4 col-md-4">
						<a href="solaras_plasma.php" class="thumbnail">
							<img src="/uploads/games/thumbs/preview.jpg" alt="">
						</a>
					</div>
					<div class="col-xs-4 col-md-4">
						<a href="qoras_court.php" class="thumbnail">
							<img src="/uploads/games/thumbs/preview2.jpg" alt="">
						</a>
					</div>
					<div class="col-xs-4 col-md-4">
						<a href="paladins_touch.php" class="thumbnail">
							<img src="/uploads/games/thumbs/preview3.jpg" alt="">
						</a>
					</div>
				</div>
				<a class="btn btn-trans btn-block" href="games.php">View All Games</a>
			</div>

			<div>
				<h1>Recent Images</h1>
				<hr class="linear_hr">
				<div class="row gallery-media">
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/images/preview.jpg" class="thumbnail" data-lightbox="imagesSet" data-title="Recent Images Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/images/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
								<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/images/preview.jpg" class="thumbnail" data-lightbox="imagesSet" data-title="Recent Images Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/images/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
								<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/images/preview.jpg" class="thumbnail" data-lightbox="imagesSet" data-title="Recent Images Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/images/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
								<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/images/preview.jpg" class="thumbnail" data-lightbox="imagesSet" data-title="Recent Images Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/images/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
								<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/images/preview.jpg" class="thumbnail" data-lightbox="imagesSet" data-title="Recent Images Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/images/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
								<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/images/preview.jpg" class="thumbnail" data-lightbox="imagesSet" data-title="Recent Images Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/images/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
								<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/images/preview.jpg" class="thumbnail" data-lightbox="imagesSet" data-title="Recent Images Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/images/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
								<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/images/preview.jpg" class="thumbnail" data-lightbox="imagesSet" data-title="Recent Images Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/images/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
								<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							</span>
						</a>
					</div>
				</div>
				<a class="btn btn-trans btn-block" href="media_images.php">View All Images</a>
			</div>

			<div>
				<h1>Action Interactives</h1>
				<hr class="linear_hr">
				<div class="row">
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="interactives_action.php" class="thumbnail">
							<span class="thumbnail_img">
								<img src="/uploads/interactives/back/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Ealsah Missionary</h5>
							</span>
						</a>
					</div>
				</div>
				<a class="btn btn-trans btn-block" href="interactives.php">View All Action Interactives</a>
			</div>

			<div>
				<h1>Strip Interactives</h1>
				<hr class="linear_hr">
				<p>Once open, mouseclick on image + scroll down to undress, and back up to dress again.</p>
				
				<div class="row">
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/characters/featured/preview.png" class="thumbnail" data-lightbox="Strip-InteractivesSet1" data-title="Strip Interactives Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Rizelle</h5>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/characters/featured/preview.png" class="thumbnail" data-lightbox="Strip-InteractivesSet2" data-title="Strip Interactives Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Zanita</h5>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/characters/featured/preview.png" class="thumbnail" data-lightbox="Strip-InteractivesSet3" data-title="Strip Interactives Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Shyael</h5>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="/uploads/characters/featured/preview.png" class="thumbnail" data-lightbox="Strip-InteractivesSet" data-title="Strip Interactives Preview Title Header">
							<span class="thumbnail_img">
								<i class="action_i __zoom"></i>
								<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Feryl</h5>
							</span>
						</a>
					</div>
				</div>
				<a class="btn btn-trans btn-block" href="interactives.php#strip">View All Strip Interactives</a>
			</div>

			<div>
				<h1>Walpapers</h1>
				<hr class="linear_hr">
				<div class="row">
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="downloads.php#walpappers" class="thumbnail">
							<span class="thumbnail_img">
								<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Divya</h5>
								<span class="rateStars rate_4">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</span>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="downloads.php#walpappers" class="thumbnail">
							<span class="thumbnail_img">
								<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Nude Elf</h5>
								<span class="rateStars rate_3">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</span>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="downloads.php#walpappers" class="thumbnail">
							<span class="thumbnail_img">
								<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Felitia</h5>
								<span class="rateStars rate_5">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</span>
							</span>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 col-md-3">
						<a href="downloads.php#walpappers" class="thumbnail">
							<span class="thumbnail_img">
								<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</span>
							<span class="caption">
								<h5 class="media_hdr">Elly</h5>
								<span class="rateStars rate_4">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</span>
							</span>
						</a>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<h1>Video Loops</h1>
						<hr class="linear_hr">
						<div class="row">
							<div class="col-xs-6 col-md-6">
								<div class="thumbnail">
									<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
										<i class="action_i __play"></i>
										<img src="/uploads/video_loops/thumbs/preview.jpg" alt="">
									</a>
									<div class="caption">
										<h5 class="media_hdr">Erdolliel</h5>
										<div class="rateStars rate_3">
											<i class="rate_i"></i>
											<i class="rate_i __on"></i>
										</div>
										<div class="formats thumb_hovered">
											<i class="formats_i __win"></i>
											<i class="formats_i __qtime"></i>
											<i class="formats_i __hd"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-md-6">
								<div class="thumbnail">
									<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
										<i class="action_i __play"></i>
										<img src="/uploads/video_loops/thumbs/preview.jpg" alt="">
									</a>
									<div class="caption">
										<h5 class="media_hdr">Trime</h5>
										<div class="rateStars rate_5">
											<i class="rate_i"></i>
											<i class="rate_i __on"></i>
										</div>
										<div class="formats thumb_hovered">
											<i class="formats_i __win"></i>
											<i class="formats_i __qtime"></i>
											<i class="formats_i __hd"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<h1>Animated Gifs</h1>
						<hr class="linear_hr">
						<div class="row">
							<div class="col-xs-6 col-md-6">
								<div class="thumbnail">
									<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="animatedSet" data-title="Animated Gifs Title Header">
										<i class="action_i __zoom"></i>
										<img src="/uploads/animated/thumbs/preview.jpg" alt="">
									</a>
									<div class="caption">
										<h5 class="media_hdr">Divya</h5>
										<div class="rateStars rate_5">
											<i class="rate_i"></i>
											<i class="rate_i __on"></i>
										</div>
										<div class="formats thumb_hovered">
											<i class="formats_i __zip"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-md-6">
								<div class="thumbnail">
									<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="animatedSet" data-title="Animated Gifs Preview Title Header">
										<i class="action_i __zoom"></i>
										<img src="/uploads/animated/thumbs/preview.jpg" alt="">
									</a>
									<div class="caption">
										<h5 class="media_hdr">Opiette</h5>
										<div class="rateStars rate_4">
											<i class="rate_i"></i>
											<i class="rate_i __on"></i>
										</div>
										<div class="formats thumb_hovered">
											<i class="formats_i __zip"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<a class="btn btn-trans btn-block" href="downloads.php">View All Downloads</a>
			</div>
		</div>

		<aside class="col-md-2 side-column">
			<h1>News</h1>
			<hr>
			<div class="side-wrapper news_aside">
				<div class="thumbnail">
					<a href="news.php" class="thumbnail_lnk"><img src="/uploads/news/thumbs/preview.jpg" alt=""></a>
					<div class="caption">
						<h5 class="news_heading">Qora's Court Pre-Release</h5>
						<p>Qora's Court is our first of many new HTML5 mini-games under development. This game is short and sweet, to introduce one of our dialogue and action control gameplay formats. <a class="read_more" href="news.php">Read more &gt;&gt;</a></p>
					</div>
				</div>
				<div class="thumbnail">
					<a href="news.php" class="thumbnail_lnk"><img src="/uploads/news/thumbs/preview.jpg" alt=""></a>
					<div class="caption">
						<h5 class="news_heading">Minigame "Solara's Plasma" Coming Soon</h5>
						<p>Join Solara the buxom Sun Mage in this interactive game of foreplay and pleasure! Help her as she offers her devotions to the sun through the pleasure of her body! <a class="read_more" href="news.php">Read more &gt;&gt;</a></p>
					</div>
				</div>
				<div class="thumbnail">
					<a href="news.php" class="thumbnail_lnk"><img src="/uploads/news/thumbs/preview.jpg" alt=""></a>
					<div class="caption">
						<h5 class="news_heading">Preview Video: Ari's Amulet</h5>
						<p>Check out a preview of our upcoming video release "Ari's Amulet". Ari has charged you with the quest to find the amulet that will allow her to escape from the plane between realms. <a class="read_more" href="news.php">Read more &gt;&gt;</a></p>
					</div>
				</div>
				<a class="btn btn-trans btn-block" href="news.php">All News</a>
			</div>
		</aside>
	</div>

	<?php include "template/modal_video_loops.php"; ?>
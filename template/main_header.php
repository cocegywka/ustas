<?php include "head.php"; ?>

<body class="<?php if(is_integer(strpos($_SERVER['PHP_SELF'], 'index.php'))): echo 'front-page'; elseif(is_integer(strpos($_SERVER['PHP_SELF'], 'join.php'))): echo 'join-page'; else: echo 'base-template'; endif; ?>" data-language="EN" data-locale="EN_US">

	<div id="main-container">
		<?php include "header.php"; ?>

		<section id="page-content" role="main">
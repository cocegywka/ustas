<article class="container inner-txt">
		<h1>A Glimpse of Felther's Heroines</h1>
	<hr>
	<ul class="ugrid">
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Felitia</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Vientra</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Trime</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Valorah</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Feryl</h3>
				</div>
			</a>
		</li>
	</ul>
	<ul class="ugrid">
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Tweyani</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr twoRows">Thalia &<br> Omyra</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Opiette</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Divya</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Hydrea</h3>
				</div>
			</a>
		</li>
	</ul>
	<ul class="ugrid">
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Terrawyn</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr twoRows">Parah &<br> Salin</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Weave</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr twoRows">Queen<br> Noscrume</h3>
				</div>
			</a>
		</li>
		<li class="ugrid-col-5">
			<a href="join.php" class="thumbnail charCol" data-toggle="tooltip" data-placement="top" data-original-title="Lorem ipsum dolor sit amet, consectetur adipisicing elit. At architecto corrupti nam laboriosam, sequi voluptas fugit, labore nihil.">
				<img src="/uploads/characters/preview.png" alt="">
				<div class="caption mask">
					<h3 class="char_hdr">Extras</h3>
				</div>
			</a>
		</li>
	</ul>
	<hr>
	<div class="row joinBar">
			<div class="col-md-offset-1 col-md-7">
				<h1 class="text-center">A World of Wonder, Magic & List Awaits You Inside</h>
			</div>
			<div class="col-md-3">
				<a href="join.php" class="btn btn-lg btn-primary btn-block">Join Now</a>
			</div>
	</div>
</article>
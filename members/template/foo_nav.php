<nav class="footer_nav">
    <div class="row-fluid">
        <div class="col-xs-6 col-xs-offset-3 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
            <ul class="footer_nav__pills nav nav-pills nav-justified">
                <li class="footer_nav__i"><a href="index.php">Home</a></li>
                <li class="footer_nav__i"><a href="characters.php">Characters</a></li>
                <li class="footer_nav__i"><a href="media.php">Media</a></li>
                <li class="footer_nav__i"><a href="games.php">Games</a></li>
                <li class="footer_nav__i"><a href="news.php">News</a></li>
            </ul>
        </div>
        <div class="col-xs-6 col-xs-offset-3 col-sm-10 col-sm-offset-1">
            <ul class="footer_nav__pills nav nav-pills nav-justified">
                <li class="footer_nav__i"><a href="terms.php">Terms & Conditions</a></li>
                <li class="footer_nav__i"><a href="privacy.php">Privacy Policy</a></li>
                <li class="footer_nav__i"><a href="https://epoch.com/billingsupport">Billing Support</a></li>
                <li class="footer_nav__i"><a href="contact.php">Contact</a></li>
            </ul>
        </div>
        <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-5 col-md-2 col-md-offset-5">
            <ul class="footer_nav__pills nav nav-pills nav-justified">
                <li class="footer_nav__i"><a class="header_gold" href="../index.php">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
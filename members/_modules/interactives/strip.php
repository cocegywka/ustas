	<h1>Strip Single</h1>
	<hr>

	<p>Rizelle the Druid feels a passion for the natural world that few can ever hope to touch, even while touching her.  Ever loving and learning from the life around her, she can imbue herself and others with the spirit of an animal to gain a temporary boost to its associated attribute.</p>

	<div class="text-center">
		<img src="/uploads/characters/featured/preview.png" alt="">
	</div>
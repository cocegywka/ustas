<article class="container inner-txt media-gallery">
	<h1>Media</h1> 
    <hr>

    <ul id="maidaTabs" class="nav nav-tabs media-nav _tabs" role="tablist">
        <li class="active">
            <a href="#videos" data-url="index.php" class="media-nav_lnk" role="tab" data-toggle="tab">Videos</a>
        </li>
        <li>
            <a href="#images" data-url="/contact.php" class="media-nav_lnk" role="tab" data-toggle="tab">Images</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade in active media-gallery" id="videos">
            some video slider
        </div>


        <div class="tab-pane fade media-gallery" id="images">
            <div id="galleria-controls" class="galleria-component clearfix">
                <ul id="galleria-content-type" class="type"><li data-type="" class="images active"></li><li data-type="" class="videos"></li></ul>

                <ul class="functions">
                    <li class="play"><span>Play</span></li>
                    <li class="download">
                        <span class="title">Download</span>
                        <ul id="gallery-dl-list">
                            <li class="640-360"><span>640 x 360 (Extra Small)</span></li>
                            <li class="1024-576"><span>1024 x 576 (Small)</span></li>
                            <li class="1280-720"><span>1280 x 720 (Middle)</span></li>
                            <li class="1920-1080"><span>1920 x 1080 (Large)</span></li>
                        </ul>
                    </li>
                    <li class="fullscreen"><span>Fullscreen</span></li>
                </ul>
            </div>

            <div id="galleria-content" class="galleria-has-age-gates">
                <ul id="media-gallery-image-list">
                    <li>
                        <a 
                        href="/"
                        data-title="Image preview 1"
                        data-description="image description here 1"
                        data-id="media.php"
                        data-tags=""
                        data-game-title="XXXELFXXX 1"
                        data-image="/uploads/images/preview.jpg"
                        data-thumb="/uploads/images/thumbs/preview.jpg"
                        data-big="/uploads/images/preview.jpg"
                        data-image-640-360="http://small"
                        data-image-1024-576="http://normal"
                        data-image-1280-720="http://hd"
                        data-image-1920-1080="http://full_hd"
                        >
                        XXXELFXXX - Images preview</a>
                    </li>
                    <li>
                        <a 
                        href="/"
                        data-title="Image preview 2"
                        data-description="image description here 2"
                        data-id="media.php"
                        data-tags=""
                        data-game-title="XXXELFXXX 2"
                        data-image="/uploads/images/preview.jpg"
                        data-thumb="/uploads/images/thumbs/preview1.jpg"
                        data-big="/uploads/images/preview.jpg"
                        data-image-640-360="http://small"
                        data-image-1024-576="http://normal"
                        data-image-1280-720="http://hd"
                        data-image-1920-1080="http://full_hd"
                        >
                        XXXELFXXX - Images preview</a>
                    </li>
                    <li>
                        <a 
                        href="/"
                        data-title="Image preview 3"
                        data-description="image description here 3"
                        data-id="media.php"
                        data-tags=""
                        data-game-title="XXXELFXXX 3"
                        data-image="/uploads/images/preview.jpg"
                        data-thumb="/uploads/images/thumbs/preview2.jpg"
                        data-big="/uploads/images/preview.jpg"
                        data-image-640-360="http://small"
                        data-image-1024-576="http://normal"
                        data-image-1280-720="http://hd"
                        data-image-1920-1080="http://full_hd"
                        >
                        XXXELFXXX - Images preview</a>
                    </li>
                    <li>
                        <a 
                        href="/"
                        data-title="Image preview 4"
                        data-description="image description here 4"
                        data-id="media.php"
                        data-tags=""
                        data-game-title="XXXELFXXX 4"
                        data-image="/uploads/images/preview.jpg"
                        data-thumb="/uploads/images/thumbs/preview3.jpg"
                        data-big="/uploads/images/preview.jpg"
                        data-image-640-360="http://small"
                        data-image-1024-576="http://normal"
                        data-image-1280-720="http://hd"
                        data-image-1920-1080="http://full_hd"
                        >
                        XXXELFXXX - Images preview</a>
                    </li>
                    <li>
                        <a 
                        href="/"
                        data-title="Image preview 5"
                        data-description="image description here 5"
                        data-id="media.php"
                        data-tags=""
                        data-game-title="XXXELFXXX 5"
                        data-image="/uploads/images/preview.jpg"
                        data-thumb="/uploads/images/thumbs/preview4.jpg"
                        data-big="/uploads/images/preview.jpg"
                        data-image-640-360="http://small"
                        data-image-1024-576="http://normal"
                        data-image-1280-720="http://hd"
                        data-image-1920-1080="http://full_hd"
                        >
                        XXXELFXXX - Images preview</a>
                    </li>
                </ul>
            </div>

            <div id="galleria-view-trigger" class="galleria-component"></div>

        </div>
    </div>
</article>
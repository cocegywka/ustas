<h1>Media</h1> 
<hr>
<ul class="nav nav-tabs media-nav _tabs" role="tablist">
	<li class="active">
		<a href="#videos" class="media-nav_lnk" role="tab" data-toggle="tab">Videos</a>
	</li>
	<li>
		<a href="#images" class="media-nav_lnk" role="tab" data-toggle="tab">Images</a>
	</li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane fade in active media-gallery" id="videos">
		<ul class="ugrid">
			<li class="ugrid-col-5">
				<a href="media_videos.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __play"></i>
						<img src="/uploads/videos/thumbs/preview.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Ari's Amulet Preview</h5>
						<p class="_txt">A preview of the upcoming video entitled "Ari's Amulet".</p>
						<div class="rateStars rate_4">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
			<li class="ugrid-col-5">
				<a href="media_videos.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __play"></i>
						<img src="/uploads/videos/thumbs/preview.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Zelwynn's Charge</h5>
						<p class="_txt">Zelwynn sucks you into the pleasures of life in Xtacia.</p>
						<div class="rateStars rate_2">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
			<li class="ugrid-col-5">
				<a href="media_videos.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __play"></i>
						<img src="/uploads/videos/thumbs/preview.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">A Sensual Faerie</h5>
						<p class="_txt">Amarok encounters a sexy sprite on a trip down an unlikely path.</p>
						<div class="rateStars rate_5">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
			<li class="ugrid-col-5">
				<a href="media_videos.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __play"></i>
						<img src="/uploads/videos/thumbs/preview.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Episode 8: Tweyani's Revenge</h5>
						<p class="_txt">The Plains Elf warrior unleashes her inner beast.</p>
						<div class="rateStars rate_1">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
			<li class="ugrid-col-5">
				<a href="media_videos.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __play"></i>
						<img src="/uploads/videos/thumbs/preview.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Elemental Eros</h5>
						<p class="_txt">Every few hundred years the elemental beings of Felther engage in a whirlwind of earth shaking erotica.</p>
						<div class="rateStars rate_3">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
		</ul>

		<div class="showMeMore-wrapper">
			<ul class="ugrid">
				<li class="ugrid-col-5">
					<a href="media_videos.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __play"></i>
							<img class="lazy" data-original="/uploads/videos/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Masturbation Extra</h5>
							<p class="_txt">A sexy elf plays with herself.</p>
							<div class="rateStars rate_1">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="media_videos.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __play"></i>
							<img class="lazy" data-original="/uploads/videos/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Masturbation Extra</h5>
							<p class="_txt">A sexy elf plays with herself.</p>
							<div class="rateStars rate_1">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="media_videos.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __play"></i>
							<img class="lazy" data-original="/uploads/videos/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Princess Walk</h5>
							<p class="_txt">Animated nude walking demo.</p>
							<div class="rateStars rate_1">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="media_videos.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __play"></i>
							<img class="lazy" data-original="/uploads/videos/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Episode 5: Feryl's Desire</h5>
							<p class="_txt">Getting filled at the border of the Great Empty.</p>
							<div class="rateStars rate_1">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="media_videos.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __play"></i>
							<img class="lazy" data-original="/uploads/videos/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Episode 4: Valorah's Blessing</h5>
							<p class="_txt">An encounter in eastern Omonia.</p>
							<div class="rateStars rate_1">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
			</ul>
		</div>
		<a class="show_more __toggle btn btn-trans btn-block"><span class="upd_ic">Show me more</span></a>
	</div>
	<div class="tab-pane fade media-gallery" id="images">
		<ul class="ugrid">
			<li class="ugrid-col-5">
				<a href="media_images.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __gallery"></i>
						<img src="/uploads/galleries/preview1.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
						<p class="_txt">Preview of minigame "Solara's Plasma".</p>
						<div class="rateStars rate_5">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
			<li class="ugrid-col-5">
				<a href="media_images.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __gallery"></i>
						<img src="/uploads/galleries/preview1.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Minigame Preview: Qora's Court</h5>
						<p class="_txt">A preview of our minigame "Qora's Court".</p>
						<div class="rateStars rate_4">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
			<li class="ugrid-col-5">
				<a href="media_images.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __gallery"></i>
						<img src="/uploads/galleries/preview1.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Strip Interactives Gallery</h5>
						<p class="_txt">Full size images of the Strip Interactives elves.</p>
						<div class="rateStars rate_3">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
			<li class="ugrid-col-5">
				<a href="media_images.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __gallery"></i>
						<img src="/uploads/galleries/preview1.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Sylafel Solo</h5>
						<p class="_txt">Magical extacy on the beach.</p>
						<div class="rateStars rate_4">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
			<li class="ugrid-col-5">
				<a href="media_images.php" class="thumbnail">
					<div class="thumbnail_img">
						<i class="action_i __gallery"></i>
						<img src="/uploads/galleries/preview1.jpg" alt="">
					</div>
					<div class="caption">
						<h5 class="media_hdr">Valorah & Oralis</h5>
						<p class="_txt">Lovemaking for you to behold.</p>
						<div class="rateStars rate_5">
							<i class="rate_i"></i>
							<i class="rate_i __on"></i>
						</div>
					</div>
				</a>
			</li>
		</ul>

		<div class="showMeMore-wrapper">
			<ul class="ugrid">
				<li class="ugrid-col-5">
					<a href="media_images.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __gallery"></i>
							<img class="lazy" data-original="/uploads/galleries/preview1.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Minigame Preview: Solara's Plasma</h5>
							<p class="_txt">Preview of minigame "Solara's Plasma".</p>
							<div class="rateStars rate_5">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="media_images.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __gallery"></i>
							<img class="lazy" data-original="/uploads/galleries/preview1.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Minigame Preview: Qora's Court</h5>
							<p class="_txt">A preview of our minigame "Qora's Court".</p>
							<div class="rateStars rate_4">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="media_images.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __gallery"></i>
							<img class="lazy" data-original="/uploads/galleries/preview1.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Strip Interactives Gallery</h5>
							<p class="_txt">Full size images of the Strip Interactives elves.</p>
							<div class="rateStars rate_3">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="media_images.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __gallery"></i>
							<img class="lazy" data-original="/uploads/galleries/preview1.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Sylafel Solo</h5>
							<p class="_txt">Magical extacy on the beach.</p>
							<div class="rateStars rate_4">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="media_images.php" class="thumbnail">
						<div class="thumbnail_img">
							<i class="action_i __gallery"></i>
							<img class="lazy" data-original="/uploads/galleries/preview1.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Valorah & Oralis</h5>
							<p class="_txt">Lovemaking for you to behold.</p>
							<div class="rateStars rate_5">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
						</div>
					</a>
				</li>
			</ul>
		</div>
		<a class="show_more __toggle btn btn-trans btn-block"><span class="upd_ic">Show me more</span></a>
	</div>
</div>
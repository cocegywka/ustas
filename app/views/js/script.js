$(document).ready(function() {

// init lazy load
$("img.lazy").lazyload({
    effect : "fadeIn",
    failure_limit : 5
});

// Init tooltips
$('.charCol').tooltip();

// pause video in modal window
  $('#modalClose').on('click', function() {
    $('#modal_trailer')[0].pause();
  })

// front page bottom content switcher
var action = 1;
$(".arrowToShow").on("click", viewSomething);
function viewSomething() {
  if ( action == 1 ) {
    $("html, body").animate({ scrollTop: $(document).height() + $('.pro-main').height()}, "slow", function() {
      $('.arrowToShow').addClass('arrowToShow_top');
    });
    action = 2;
    $('.pro-main').slideDown('slow');

  } else {
    $('html, body').animate({scrollTop: 0}, 'slow', function() {
      $('.arrowToShow').removeClass('arrowToShow_top');
    });
    action = 1;
    $('.pro-main').slideUp('slow');
    $(this).addClass('arrowToShow_top');
  }
}

// sound music switcher
$('.musicToggle').on('click', function() {
  $(this).children('span').toggleClass('sound_off');
})

// front banner video height
var setBannerHeight = function () {
  var height = $(window).height();
  $('.front-banner').css('height', (height));
};

$(window).on("resize", function () {
  setBannerHeight();
}).resize();

// view trailer bottom tab toggle
$('.featureTabHeader, .features_i').hover(function() {

  $(this).next().toggleClass('features_li__focus');
  $(this).toggleClass('features_li__focus');
})

// collapse navbar
$('.navbar-toggle').on('click', function() {

  $('.navbar-collapse').toggleClass('open');

  if($('#headerNavbarCollapse').hasClass('open')) {
    $('.navbar-toggle').addClass('hovered');
  } else {
    $('.navbar-toggle').removeClass('hovered');
  }

})

/* -==Member styles==- */

var $thumbs = $('.thumbnail');
$('.thumbnail').hover(function(){

  $(this).each(function(){
    if($(this).find('select').is(":focus")) {
        $(this).addClass('active');
    } else {
        $(this).removeClass('active');
    }

  })
});

// show more toggle
$('.show_more.__toggle').on('click', function() {
  $(this).siblings('.showMeMore-wrapper').slideToggle('slow');
})

// Tabs
  // cache the id, url history
  var navbox = $('.nav-tabs');
  // activate tab on click
  navbox.on('click', 'a', function (e) {
    var $this = $(this);
    // prevent the Default behavior
    e.preventDefault();
    // set the hash to the address bar
    window.location.hash = $this.attr('href');
    // activate the clicked tab
    $this.tab('show');
  })

  // if we have a hash in the address bar
  if(window.location.hash){
    // show right tab on load (read hash from address bar)
    navbox.find('a[href="'+window.location.hash+'"]').tab('show');
  }

});
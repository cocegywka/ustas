	<h1>Interactives</h1>
	<hr>

	<ul class="nav nav-tabs media-nav _tabs" role="tablist">
		<li class="active">
			<a href="#action" class="media-nav_lnk" role="tab" data-toggle="tab">Action</a>
		</li>
		<li>
			<a href="#strip" class="media-nav_lnk" role="tab" data-toggle="tab">Strip</a>
		</li>
		<li>
			<a href="#trophy_girls" class="media-nav_lnk" role="tab" data-toggle="tab">Trophy Girls</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane fade in active" id="action">
			<ul class="ugrid">
				<li class="ugrid-col-5">
					<a href="interactives_action.php" class="thumbnail">
						<div class="thumbnail_img">
							<img src="/uploads/interactives/back/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Ealsah Missionary</h5>
						</div>
					</a>
				</li>
			</ul>
		</div>
		<div class="tab-pane fade" id="strip">
			<ul class="ugrid">
				<li class="ugrid-col-5">
					<a href="interactives_strip.php" class="thumbnail">
						<div class="thumbnail_img">
							<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Rizelle</h5>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="interactives_strip.php" class="thumbnail">
						<div class="thumbnail_img">
							<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Zanita</h5>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="interactives_strip.php" class="thumbnail">
						<div class="thumbnail_img">
							<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Shyael</h5>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="interactives_strip.php" class="thumbnail">
						<div class="thumbnail_img">
							<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Feryl</h5>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a href="interactives_strip.php" class="thumbnail">
						<div class="thumbnail_img">
							<img src="/uploads/characters/featured/thumbs/preview.jpg" alt="">
						</div>
						<div class="caption">
							<h5 class="media_hdr">Gladshewyll</h5>
						</div>
					</a>
				</li>
			</ul>
		</div>
		<div class="tab-pane fade" id="trophy_girls">
			Trophy girls content
		</div>
	</div>
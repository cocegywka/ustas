<article class="container inner-txt">
    <form id="loginForm" class="form-tmp center-block" action="members/index.php" role="form">
        <h1 class="text-center">Please login in</h1>
        <hr>
        <label for="Email address">Email address:</label>
        <input type="email" class="form-control" required="" autofocus="">
        <label for="Password">Password:</label>
        <input type="password" class="form-control" required="">
        <a class="lnk" href="https://epoch.com/billingsupport">Login trouble? Contact Billing Support </a>
        <label class="checkbox">
          <input class="form_checkbox" type="checkbox" value="remember-me"> Remember me
      </label>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <div class="text-center">
         <a class="lnk" href="join.php">Not a member? Create an account!</a>
     </div>
 </form>
</article>
	<h1>Games</h1>
	<hr>

	<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
    <div class="thumbnail view view-to-top">
      <img src="/uploads/games/thumbs/preview.jpg" alt="">
      <div class="caption mask">
        <h3 class="view_hdr">Game title example</h3>
        <p class="view_txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, illo, aut. Molestias velit reiciendis cumque, porro voluptatum tempore commodi provident.</p>
        <div class="view_btnGroup">
          <a href="solaras_plasma.php" class="btn btn-warning" role="button">Play Now</a>
          <a href="#" class="btn btn-invert" role="button">More Info</a>
          <a href="#" class="share-i"></a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-md-4">
    <div class="thumbnail view view-to-top">
      <img src="/uploads/games/thumbs/preview2.jpg" alt="">
      <div class="caption mask">
        <h3 class="view_hdr">Game title example</h3>
        <p class="view_txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, illo, aut. Molestias velit reiciendis cumque, porro voluptatum tempore commodi provident.</p>
        <div class="view_btnGroup">
          <a href="qoras_court.php" class="btn btn-warning" role="button">Play Now</a>
          <a href="#" class="btn btn-invert" role="button">More Info</a>
          <a href="#" class="share-i"></a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-6 col-md-4">
    <div class="thumbnail view view-to-top">
      <img src="/uploads/games/thumbs/preview3.jpg" alt="">
      <div class="caption mask">
        <h3 class="view_hdr">Game title example</h3>
        <p class="view_txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, illo, aut. Molestias velit reiciendis cumque, porro voluptatum tempore commodi provident.</p>
        <div class="view_btnGroup">
          <a href="paladins_touch.php" class="btn btn-warning" role="button">Play Now</a>
          <a href="#" class="btn btn-invert" role="button">More Info</a>
          <a href="#" class="share-i"></a>
        </div>
      </div>
    </div>
  </div>
</div>
<article class="front-banner">
    <div class="musicToggle">
        <span class="musicToggle_i"></span>
    </div><!-- /.musicToggle -->

    <div class="banner_action">
        <button id="viewTrailer" class="banner_trailer__btn btn btn-default btn-lg" data-toggle="modal" data-target="#trailerModal">
            <span class="viewTrailer_but"></span> View Trailer
        </button>
    </div><!-- /.banner_action -->

    <!-- Toggle bottom content -->
    <a class="arrowToShow btn"><i class="arrowToShow_i"></i></a>

    <div class="video_wrapper">
        <video id="videobcg" preload="auto" autoplay="true" loop="loop" muted="muted" class="hidden-vd-xs">
            <source src="uploads/videos/home.mp4" type="video/mp4">
            <source src="uploads/videos/home.webm" type="video/webm">
            <img class="background-img" width="1920" height="100%" src="app/views/images/tour_banner.jpg" alt="background" data-anchor-target="#intro">
            Sorry, your browser does not support HTML5 video.
        </video>

                <div class="media__grid"></div><!-- /.video_wrapper -->
        </article>

        <!--<div id="features-wrapper">
                <ul class="features-ul">
                    <li class="features_li">
                        <a href="#" id="viewTrailer" class="hoverToggle" data-toggle="modal" data-target="#trailerModal">
                            <h3 class="featureTabHeader trailer_button"><small>View</small> Trailer</h3>
                            <div class="features_i tralier_view">
                                <div class="asideInfo">
                                    <span class="asideInfo__span" href="#">click here</span>
                                    <h4 class="asideInfo_hdr">to watch xxxelfxxx trailer</h4>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>-->
            <!-- /.#features-wrapper -->

        <div class="pro-main">
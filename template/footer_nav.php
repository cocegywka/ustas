<nav class="footer_nav">
    <div class="row-fluid">
        <div class="col-xs-6 col-xs-offset-3 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2">
            <ul class="footer_nav__pills nav nav-pills nav-justified">
                <li class="footer_nav__i"><a href="/index.php">Home</a></li>
                <li class="footer_nav__i"><a href="/xxxelf_preview.php">Preview</a></li>
                <li class="footer_nav__i"><a href="/login.php">Login</a></li>
                <li class="footer_nav__i"><a href="/dmca_agent.php">DMCA Agent</a></li>
                <li class="footer_nav__i"><a href="/terms.php">Terms & Conditions</a></li>
                <li class="footer_nav__i"><a href="/privacy.php">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="col-xs-6 col-xs-offset-3 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <ul class="footer_nav__pills nav nav-pills nav-justified">
                <li class="footer_nav__i"><a href="/affiliate_program.php">Affiliate Program</a></li>
                <li class="footer_nav__i"><a href="/contact.php">Contact</a></li>
                <li class="footer_nav__i"><a href="https://epoch.com/billingsupport">Billing Support</a></li>
            </ul>
        </div>
        <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-5 col-md-2 col-md-offset-5">
            <ul class="footer_nav__pills nav nav-pills nav-justified">
                <li class="footer_nav__i"><a class="header_gold" href="/join.php">Join Now</a></li>
            </ul>
        </div>
    </div>
</nav>
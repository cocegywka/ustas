<div class="pageHeader">
		<h1 class="pageHeader_hdr">Downloads</h1>
		<div class="elfModel"></div>
</div>

<div class="container inner_txt pageHeader_pad">
	<ul class="nav nav-tabs media-tabs" role="tablist">
		<li class="active">
			<a href="#walpappersTab" class="media-tabs_lnk" role="tab" data-toggle="tab">Wallpappers</a>
		</li>
		<li>
			<a href="#video_loopsTab" class="media-tabs_lnk" role="tab" data-toggle="tab">Video Loops</a>
		</li>
		<li>
			<a href="#animatedTab" class="media-tabs_lnk" role="tab" data-toggle="tab">Animated Gif</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane fade in active" id="walpappersTab">
			<ul class="ugrid">
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview1.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Divya</h5>
							<div class="rateStars rate_4">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview2.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Nude Elf</h5>
							<div class="rateStars rate_2">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview3.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Felitia</h5>
							<div class="rateStars rate_5">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview4.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Elly</h5>
							<div class="rateStars rate_1">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview5.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Opiette</h5>
							<div class="rateStars rate_3">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
			</ul>
			<ul class="ugrid">
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview1.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Divya</h5>
							<div class="rateStars rate_1">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview2.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Nude Elf</h5>
							<div class="rateStars rate_5">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview3.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Felitia</h5>
							<div class="rateStars rate_3">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview4.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Elly</h5>
							<div class="rateStars rate_2">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
				<li class="ugrid-col-5">
					<a class="thumbnail">
							<img src="assets/img/thumb/video_preview5.jpg" alt="">
						<div class="caption">
							<h5 class="media_hdr">Opiette</h5>
							<div class="rateStars rate_4">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</a>
				</li>
			</ul>
		</div>
		<div class="tab-pane fade" id="video_loopsTab">
			<br>
			Video Loops content

		</div>
		<div class="tab-pane fade" id="animatedTab">
			<br>
			Animated Gif content

		</div>
	</div>
		<a class="show_more btn btn-primary btn-block" href="#"><span class="upd_ic">Show me more</span></a>
</div>
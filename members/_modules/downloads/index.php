	<h1>Downloads</h1>
	<hr>

	<ul class="nav nav-tabs media-nav" role="tablist">
		<li class="active">
			<a href="#walpappers" class="media-nav_lnk" role="tab" data-toggle="tab">Wallpappers</a>
		</li>
		<li>
			<a href="#video_loops" class="media-nav_lnk" role="tab" data-toggle="tab">Video Loops</a>
		</li>
		<li>
			<a href="#animated_gif" class="media-nav_lnk" role="tab" data-toggle="tab">Animated Gif</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane fade in active" id="walpappers">
			<ul class="ugrid">
				<li class="ugrid-col-5">
					<div class="thumbnail">
						<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
							<i class="action_i __zoom"></i>
							<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
						</a>
						<div class="caption">
							<h5 class="media_hdr">Divya</h5>
							<div class="rateStars rate_4">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="ugrid-col-5">
					<div class="thumbnail">
						<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
							<i class="action_i __zoom"></i>
							<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
						</a>
						<div class="caption">
							<h5 class="media_hdr">Nude Elf</h5>
							<div class="rateStars rate_2">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="ugrid-col-5">
					<div class="thumbnail">
						<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
							<i class="action_i __zoom"></i>
							<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
						</a>
						<div class="caption">
							<h5 class="media_hdr">Felitia</h5>
							<div class="rateStars rate_5">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="ugrid-col-5">
					<div class="thumbnail">
						<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
							<i class="action_i __zoom"></i>
							<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
						</a>
						<div class="caption">
							<h5 class="media_hdr">Elly</h5>
							<div class="rateStars rate_1">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="ugrid-col-5">
					<div class="thumbnail">
						<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
							<i class="action_i __zoom"></i>
							<img src="/uploads/wallpaper/thumbs/preview.jpg" alt="">
						</a>
						<div class="caption">
							<h5 class="media_hdr">Opiette</h5>
							<div class="rateStars rate_3">
								<i class="rate_i"></i>
								<i class="rate_i __on"></i>
							</div>
							<div class="dwn_controls thumb_hovered row">
								<div class="col-md-7">
									<select name="wallpapper_size" class="form-control">
										<option value="800x600">800x600</option>
										<option value="1280x980">1280x980</option>
										<option value="1280x1024">1280x1024</option>
										<option value="1920x1080">1920x1080</option>
									</select>
								</div>
								<div class="col-md-5 hovered_btn">
									<button type="button" class="btn btn-default btn-xs">Download</button>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>

			<div class="showMeMore-wrapper">
				<ul class="ugrid">
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
								<i class="action_i __zoom"></i>
								<img class="lazy" data-original="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">Divya</h5>
								<div class="rateStars rate_1">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
								<div class="dwn_controls thumb_hovered row">
									<div class="col-md-7">
										<select name="wallpapper_size" class="form-control">
											<option value="800x600">800x600</option>
											<option value="1280x980">1280x980</option>
											<option value="1280x1024">1280x1024</option>
											<option value="1920x1080">1920x1080</option>
										</select>
									</div>
									<div class="col-md-5 hovered_btn">
										<button type="button" class="btn btn-default btn-xs">Download</button>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
								<i class="action_i __zoom"></i>
								<img class="lazy" data-original="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">Nude Elf</h5>
								<div class="rateStars rate_5">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
								<div class="dwn_controls thumb_hovered row">
									<div class="col-md-7">
										<select name="wallpapper_size" class="form-control">
											<option value="800x600">800x600</option>
											<option value="1280x980">1280x980</option>
											<option value="1280x1024">1280x1024</option>
											<option value="1920x1080">1920x1080</option>
										</select>
									</div>
									<div class="col-md-5 hovered_btn">
										<button type="button" class="btn btn-default btn-xs">Download</button>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
								<i class="action_i __zoom"></i>
								<img class="lazy" data-original="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">Felitia</h5>
								<div class="rateStars rate_3">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
								<div class="dwn_controls thumb_hovered row">
									<div class="col-md-7">
										<select name="wallpapper_size" class="form-control">
											<option value="800x600">800x600</option>
											<option value="1280x980">1280x980</option>
											<option value="1280x1024">1280x1024</option>
											<option value="1920x1080">1920x1080</option>
										</select>
									</div>
									<div class="col-md-5 hovered_btn">
										<button type="button" class="btn btn-default btn-xs">Download</button>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
								<i class="action_i __zoom"></i>
								<img class="lazy" data-original="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">Elly</h5>
								<div class="rateStars rate_2">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
								<div class="dwn_controls thumb_hovered row">
									<div class="col-md-7">
										<select name="wallpapper_size" class="form-control">
											<option value="800x600">800x600</option>
											<option value="1280x980">1280x980</option>
											<option value="1280x1024">1280x1024</option>
											<option value="1920x1080">1920x1080</option>
										</select>
									</div>
									<div class="col-md-5 hovered_btn">
										<button type="button" class="btn btn-default btn-xs">Download</button>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/wallpaper/preview.jpg" class="thumbnail_img" data-lightbox="wallpapersSet" data-title="Walpapers Preview Title Header">
								<i class="action_i __zoom"></i>
								<img class="lazy" data-original="/uploads/wallpaper/thumbs/preview.jpg" alt="">
							</a>
							<div class="caption">
								<h5 class="media_hdr">Opiette</h5>
								<div class="rateStars rate_4">
									<i class="rate_i"></i>
									<i class="rate_i __on"></i>
								</div>
								<div class="dwn_controls thumb_hovered row">
									<div class="col-md-7">
										<select name="wallpapper_size" class="form-control">
											<option value="800x600">800x600</option>
											<option value="1280x980">1280x980</option>
											<option value="1280x1024">1280x1024</option>
											<option value="1920x1080">1920x1080</option>
										</select>
									</div>
									<div class="col-md-5 hovered_btn">
										<button type="button" class="btn btn-default btn-xs">Download</button>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<p class="clearfix"></p>
			<a class="show_more __toggle btn btn-trans btn-block"><span class="upd_ic">Show me more</span></a>
		</div>

		<div class="tab-pane fade" id="video_loops">
			<ul class="ugrid">
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img src="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Erdolliel</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img src="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img src="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img src="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img src="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
				</ul>

				<div class="showMeMore-wrapper">
					<ul class="ugrid">
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img class="lazy" data-original="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Erdolliel</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img class="lazy" data-original="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img class="lazy" data-original="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img class="lazy" data-original="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a class="thumbnail_img" data-toggle="modal" data-target="#videoLoops">
									<i class="action_i __play"></i>
									<img class="lazy" data-original="/uploads/video_loops/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __win"></i>
										<i class="formats_i __qtime"></i>
										<i class="formats_i __hd"></i>
									</div>
							</div>
						</div>
					</li>
				</ul>
				</div>
			<a class="show_more __toggle btn btn-trans btn-block"><span class="upd_ic">Show me more</span></a>
		</div>

		<div class="tab-pane fade" id="animated_gif">
			<ul class="ugrid">
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img src="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Erdolliel</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img src="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img src="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img src="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img src="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
				</ul>

				<div class="showMeMore-wrapper">
					<ul class="ugrid">
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img class="lazy" data-original="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Erdolliel</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img class="lazy" data-original="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img class="lazy" data-original="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img class="lazy" data-original="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
					<li class="ugrid-col-5">
						<div class="thumbnail">
							<a href="/uploads/animated/preview.jpg" class="thumbnail_img" data-lightbox="Animated-Gifs" data-title="Animated Gifs Preview Title Header">
									<i class="action_i __zoom"></i>
									<img class="lazy" data-original="/uploads/animated/thumbs/preview.jpg" alt="">
								</a>
							<div class="caption">
									<h5 class="media_hdr">Trime</h5>
									<div class="rateStars rate_3">
										<i class="rate_i"></i>
										<i class="rate_i __on"></i>
									</div>
									<div class="formats thumb_hovered">
										<i class="formats_i __zip"></i>
									</div>
							</div>
						</div>
					</li>
				</ul>
				</div>
			<p class="clearfix"></p>
			<a class="show_more __toggle btn btn-trans btn-block"><span class="upd_ic">Show me more</span></a>
		</div>
	</div>

	<?php include "template/modal_video_loops.php"; ?>
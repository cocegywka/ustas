<!--{ALL javascript}-->
<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/lib/jquery-1.11.1.min.js"><\/script>')</script>-->
<script src="/app/views/js/lib/jquery-1.11.1.min.js"></script>
<script>
    less = {
        env: "production",
        async: false,
        fileAsync: false,
        poll: 100,
        functions: {},
        dumpLineNumbers: false,
        relativeUrls: false,
        rootpath: "/"
    };
</script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.7.3/less.min.js"></script> -->
<script src="/app/views/js/lib/less.min.js"></script>
<script src="/app/views/js/modal.js"></script>
<script src="/app/views/js/lib/lazy.load.js"></script>
<script src="/app/views/js/lib/lightbox.min.js"></script>
<script src="/app/views/js/tab.js"></script>
<script src="/app/views/js/tooltip.js"></script>
<script src="/app/views/js/transition.js"></script>
	<h1>2257 Record-Keeping</h1>
	<hr>

	<p>18 U.S.C. Section 2257 Compliance Notice</p>

	<p>The actors, models, actresses and other persons that appear in any visual depiction of actual sexually explicit conduct appearing or otherwise contained in this web site were over the age of eighteen years at the time of the creation of such.</p>

	<p>The original records required from the primary or secondary producer of said video and visual web site media and pursuant to 18 U.S.C. section 2257 and 28 C.F.R. 75 for all materials contained in the web site are kept and are available for inspection by the following Custodian of Records:</p>

	<p>For The Birds Productions</p>

	<address>23090 <strong>Oak Leaf Lane</strong><br> Idyllwild <abbr title="State">CA</abbr> 92549</address>
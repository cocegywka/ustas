<? $btn_styles = array('nav_deffault' => '','nav_active' => 'active');?>

<nav id="primary-nav" class="navbar" role="navigation">
    <div class="container primary-nav_inner">
        <div class="row-fluid rel">
            <div id="socLinks" class="socMenu col-xs-6 col-xs-offset-6 col-sm-4 col-sm-offset-8 col-md-3 col-md-offset-9 text-right">
                <a href="#" class="socMenu_i __fc"></a>
                <a href="#" class="socMenu_i __twr"></a>
                <a href="#" class="socMenu_i __tw"></a>
            </div><!-- end/#socialBar -->
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="logo">
                    <a class="logo_lnk" href="index.php">XXXELFXXX - Sexcraft</a>
                </div>
            </div><!-- end/.navbar-header -->
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="headerNavbarCollapse">
                <ul class="nav navbar-nav navbar-right">
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'index')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$btn_styles[$btn_key];?>"><a href="index.php">Home</a></li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'characters')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$btn_styles[$btn_key];?>"><a href="characters.php">Characters</a></li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'media')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i dropdown <?=$btn_styles[$btn_key];?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Media <span class="caret"></span></a>
                        <ul class="dropdown-menu __two_it" role="menu">
                            <li><a href="media.php#videos">Videos</a></li>
                            <li><a href="media.php#images">Images</a></li>
                        </ul>
                    </li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'interactives')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i dropdown <?=$btn_styles[$btn_key];?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Interactives <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="interactives.php#action">Action</a></li>
                            <li><a href="interactives.php#strip">Strip</a></li>
                            <li><a href="interactives.php#trophy_girls">Trophy Girls</a></li>
                        </ul>
                    </li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'games')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$btn_styles[$btn_key];?>"><a href="games.php">Games</a></li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'downloads')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i dropdown <?=$btn_styles[$btn_key];?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Downloads <span class="caret"></span></a>
                        <ul class="dropdown-menu tab-content" role="menu">
                            <li><a href="downloads.php#walpappers">Wallpapers</a></li>
                            <li><a href="downloads.php#video_loops">Video Loops</a></li>
                            <li><a href="downloads.php#animated_gif">Animated Gifs</a></li>
                        </ul>
                    </li>
                    <? $btn_key = (is_integer(strpos($_SERVER['PHP_SELF'], 'news')))?'nav_active':'nav_deffault'; ?>
                    <li class="navbar_i <?=$btn_styles[$btn_key];?>"><a href="news.php">news</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </div>
</nav>

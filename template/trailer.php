<!-- Modal -->
<div class="modal fade" id="trailerModal" tabindex="-1" role="dialog" aria-labelledby="trailerModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" id="modalClose" class="close" data-dismiss="modal">
          <span aria-hidden="true">&times;</span>
          <span class="sr-only">Close</span>
        </button>
      </div>
      
      <div class="modal-body">
        <div class="embed-responsive embed-responsive-16by9">
            <video id="modal_trailer" class="modal_trailer_video" controls class="hidden-xs">
            <source src="uploads/videos/preview.mp4" type="video/mp4">
            <source src="uploads/videos/preview.webm" type="video/webm">
            Sorry, your browser does not support HTML5 video.
        </video>
        </div>
      </div>

    </div>
  </div>
  </div>
<article class="container inner-txt contact-page">
  <div class="inner-bg">
   <p class="p_heading halfWidth">Use the form below for your suggestions and inquiries. For billing and login issues please contact <a href="https://epoch.com/billingsupport">billing support</a>.</p>

   <form class="form-tmp" role="form" autocomplete="on" method="post" action="thank_you.php">
    <h1 class="text-center">Contact Us</h1>
    <hr>
    <small>*Required Fields:</small>

    <div class="row">
      <div class="col-md-6">
        <label for="Username">Username:</label>
        <input type="text" class="form-control" autofocus="">
      </div>
      <div class="col-md-6">
        <label for=""><span class="text-danger">*</span> Name:</label>
        <input type="text" class="form-control" required>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <label for=""><span class="text-danger">*</span> Email Address:</label>
        <input type="email" class="form-control" required>
      </div>
      <div class="col-md-6">
        <label for="enquiry">Type of Enquiry:</label>
        <select class="form-control">
          <option>General Inquiry</option>
          <option>Report a Problem</option>
          <option>Comments and Suggestions</option>
          <option>Other</option>
        </select>
      </div>
    </div>

    <label for="message"><span class="text-danger">*</span> Your Message:</label>
    <textarea class="form-control" rows="7" required></textarea>

    <div class="row">
      <div class="col-xs-7 col-md-4">
        <label for="type code"><span class="text-danger">*</span> Type the code:</label>
        <input type="text" class="form-control" required>
      </div>
      <div class="col-xs-5 col-md-4">
        <img class="capcha_pic" src="http://placehold.it/190x80" alt="capcha" width="100%">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4">
        <button class="btn-mrgTop btn btn-lg btn-primary btn-block" type="submit">Submit</button>
      </div>
    </div>
  </form>

</div>
</article>